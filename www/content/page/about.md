---
title: About this project
subtitle: Why you'd need to use asciidoc syntax in LabVIEW
comments: true
---

This project was born in mid 2019 in order to be use in [Antidoc project](https://wovalab.gitlab.io/open-source/labview-doc-generator/). 
Asciidoc syntax allows us to delegate the complex task of rendering final file in a modern and readable format to Asciidoctor toolchain. As we don't need to bother with this, we can focus on other LabVIEW task developments. 


### About AsciiDoc

From  [AsciiDoc](http://www.methods.co.nz/asciidoc/index.html#_introduction)

> AsciiDoc is a text document format for writing notes, documentation, articles, books, ebooks, slideshows, web pages, man pages and blogs. AsciiDoc files can be translated to many formats including HTML, PDF, EPUB, man page.

### About Asciidoctor

From [Asciidoctor](https://asciidoctor.org/)

> Asciidoctor is a fast, open source text processor and publishing toolchain for converting AsciiDoc content to HTML5, DocBook, PDF, and other formats. 

### Project Maintainer

[Olivier Jourdan](https://www.linkedin.com/in/jourdanolivier/) from [Wovalab](https://www.wovalab.com)

### Project Contributors

[Joerg Hampel](https://www.linkedin.com/in/joerghampel/) from [Hampel Software Engineering](https://www.hampel-soft.com/)

[Sam Taggart](https://www.linkedin.com/in/taggartsam/) from [System Automation Solutions LLC](https://www.sasworkshops.com/)